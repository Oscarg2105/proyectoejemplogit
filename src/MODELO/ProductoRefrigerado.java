/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author CARLOS
 */
public class ProductoRefrigerado extends Producto{

    public ProductoRefrigerado() {
    }

    public ProductoRefrigerado(String nombre, String id, double temperatura, double valorBase) {
        super(nombre, id, temperatura, valorBase);
    }

    
    @Override
    public double calcularCostoDeAlmacenamiento() {
        return getValorBase()*1.2;
    }

//    @Override
//    public String nuevoMetAbstracto() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    
}
