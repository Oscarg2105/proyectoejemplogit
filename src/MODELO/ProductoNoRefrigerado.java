/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;


/**
 *
 * @author CARLOS
 */
public class ProductoNoRefrigerado extends Producto {

    public ProductoNoRefrigerado() {
    }

    public ProductoNoRefrigerado(String nombre, String id, double temperatura, double valorBase) {
        super(nombre, id, temperatura, valorBase);
    }

    @Override
    public double calcularCostoDeAlmacenamiento() {
        double calculoCosto;
        calculoCosto= getValorBase()*0.1+getValorBase();
        return calculoCosto;//getValorBase()*1.1;
    } 

//    @Override
//    public String nuevoMetAbstracto() {
//        return "Metodo abstracto de producto, sobreescrito por la sublace producto no refrigerado";
//    }

     
   
}
