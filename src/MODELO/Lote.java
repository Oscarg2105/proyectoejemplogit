/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

import java.util.ArrayList;
import java.util.List;


public class Lote {
    
    private List<Producto> productos;

    public Lote() {
        this.productos= new ArrayList<>();
    }
    
    public void agregarProducto(Producto producto){
        this.productos.add(producto);
    }
    
    public void mostrarProductos(){
        for (Producto producto : productos) {
            System.out.println(producto);
        }
    }

    @Override
    public String toString() {
        return "Lote{" + "productos=" + productos.toString() + '}';
    }
    
    
}
