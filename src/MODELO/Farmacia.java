/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

import java.util.ArrayList;
import java.util.List; 

/**
 *
 * @author CARLOS
 */
public class Farmacia {
    
    private String nit;
    private String nombre;
    private String direccion;
    private final List<Almacen> almacenes;

    public Farmacia() {
        this.almacenes = new ArrayList<>();
    }
    
    public void agregarAlmacen(Almacen a){
        almacenes.add(a);
    }
    
    public void agregarProducto(String codigoAlmacen, Producto p){
        for (Almacen almacen : almacenes) {
            if(almacen.getCodigo().equals(codigoAlmacen)){
                almacen.agregarProducto(p);
            } else {
                System.out.println("Almacen no encontrado");
                
            }
        }
    }
    
}
