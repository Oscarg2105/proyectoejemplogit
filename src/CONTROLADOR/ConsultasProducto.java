/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLADOR;

import MODELO.Producto;
import MODELO.ProductoNoRefrigerado;
import MODELO.ProductoRefrigerado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author CARLOS
 */
public class ConsultasProducto extends ConexionBD{
    
    
    String sentenciasql;
    
    public boolean guardarProducto(Producto productos){
        ConexionBD conexion= new ConexionBD();
        sentenciasql="INSERT INTO productos (nombre,id,temperatura,valorBase)\n"
                + "values('"+productos.getNombre()+ "', '"+productos.getId()+ "',"
                + " "+productos.getTemperatura()+ ","+productos.getValorBase()+ " ); ";
        if(conexion.setAutoCommitBD(false)){
            if(conexion.insertarBD(sentenciasql)){
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
                conexion.cerrarConexion();
                return false;
        }
    }
    
    public boolean actualizarProducto(Producto producto){
        ConexionBD conexion = new ConexionBD();
        sentenciasql ="update productos set "
                + "nombre='"+producto.getNombre()+"', temperatura = "+producto.getTemperatura()+","
                + " valorBase= "+producto.getValorBase()+" where id ='"+producto.getId()+ "' ; ";
        if(conexion.setAutoCommitBD(false)){
            if(conexion.actualizarBD(sentenciasql)){
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
                conexion.cerrarConexion();
                return false;
        }
    }
    
    public boolean borrarProducto(String id){
        ConexionBD conexion= new ConexionBD();
        sentenciasql ="DELETE FROM productos where id= '"+id+"';" ; 
        if(conexion.setAutoCommitBD(false)){
            if(conexion.borrarBD(sentenciasql)){
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
                conexion.cerrarConexion();
                return false;
        }
    }
    
    public Producto getProducto(String id){
        ConexionBD conexion = new ConexionBD();
        Producto objProducto= new Producto();
        sentenciasql="select * from productos where id ='"+id+"' ;";
         
        ResultSet resultSet = conexion.consultarBD(sentenciasql);
        try{
            if(resultSet.next()){
                double tempDB= resultSet.getDouble("temperatura");
                if(tempDB<=20){
                    objProducto=new ProductoRefrigerado();
                } else {
                    objProducto = new ProductoNoRefrigerado();
                }
                objProducto.setId(resultSet.getString("id"));
                objProducto.setNombre(resultSet.getString("nombre"));
                objProducto.setTemperatura(tempDB);
                objProducto.setValorBase(resultSet.getDouble("valorBase"));
                conexion.cerrarConexion();
            } else {
                conexion.cerrarConexion();
                System.out.println("No existe el producto con es id "+id);
                return null;
            }
        }catch(SQLException ex){
            System.out.println("consulta select error: "+ex.getMessage());
        }
        System.out.println(""+objProducto);
        return objProducto;
    }
    
    public List<Producto> listarProductos(){
        ConexionBD conexion = new ConexionBD();
        List<Producto> listaProductos = new ArrayList<>();
        sentenciasql= "SELECT * FROM productos ;";//opcional order by temperatura desc
        ResultSet resultSet= conexion.consultarBD(sentenciasql);
        Producto objProducto;
        try {
            while(resultSet.next()){
                double tempDB= resultSet.getDouble("temperatura");
                if(tempDB<=20){
                    objProducto=new ProductoRefrigerado();
                } else {
                    objProducto = new ProductoNoRefrigerado();
                }
                objProducto.setId(resultSet.getString("id"));
                objProducto.setNombre(resultSet.getString("nombre"));
                objProducto.setTemperatura(tempDB);
                objProducto.setValorBase(resultSet.getDouble("valorBase"));
                listaProductos.add(objProducto);
            }
        } catch (SQLException e) {
            System.out.println("Error al consultar toda la tabla productos "+e.getSQLState());
        }
        conexion.cerrarConexion();
        return listaProductos;
    }
}
